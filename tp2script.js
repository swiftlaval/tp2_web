let col_vide;
let ligne_vide;


function create_game(im_height, im_width,nbligne,nbcol) {
    //initialisation du jeu
    let main_game = document.getElementById("main_game");
    
    let num_case = 1;
    for (i = 0; i < nbligne; i++) {


        for (j = 0; j < nbcol; j++) {
            let temp = document.createElement("div");
            temp.id = "pos_" + i + "_" + j;
            temp.title = "" + num_case;

            $(temp).css("height", "" + (im_height / nbligne));
            $(temp).css("width", "" + (im_width / nbcol));
            temp.className = "tile";

            let temp_img_content = document.createElement("img");
            temp_img_content.id = "image_pos_" + i + "_" + j;
            temp_img_content.alt = "image_pos_" + i + "_" + j;
            temp_img_content.src = document.getElementById("url").value;
            temp_img_content.title = "" + num_case;
            num_case++;

            let bottom = (im_height / nbligne) * (nbligne - (i + 1));
            let right = (im_width / nbcol) * (nbcol - (j + 1));
            let top = im_height - ((im_height / nbligne) + bottom);
            let left = im_width - ((im_width / nbcol) + right);

            
            $(temp_img_content).css("margin", "-" + top + "px -" + right + "px -" + bottom + "px -" + left + "px");
            $(temp).css("grid-row", i + 1 + "");
            $(temp).css("grid-column", j + 1 + "");
            
            temp.appendChild(temp_img_content);
            main_game.appendChild(temp);


        }
    }

}

function main() {

    let main_node = document.getElementById("main_game");
    while (main_node.firstChild) {
        main_node.removeChild(main_node.firstChild);
    }
    let nbligne = document.getElementById("nbligne").value;
    let nbcol = nbligne;
    
    col_vide = nbcol - 1;
    ligne_vide = nbligne - 1;

    let img = new Image();
    img.src = document.getElementById("url").value;
    img.onload = function () {

        let height = this.height;
        let width = this.width;
        create_game(height, width,nbligne,nbcol);
        document.getElementById("image_pos_" + (ligne_vide) + "_" + (col_vide)).remove();
        give_movement_right(col_vide, ligne_vide);

    }

}


function move(target_div) {
    //verifier si la case vers laquelle on se deplace est vide et colle a une case vide, sinon ne rien faire
    //alert("MOVEMENT en cours!!!!");
    let destination_div_id = "pos_" + ligne_vide + "_" + col_vide;

    let dest_div = document.getElementById(destination_div_id);
    if (!dest_div.hasChildNodes()) {

        //remove right
        remove_movement_right(col_vide, ligne_vide);
        //update pos vide
        
        col_vide = parseInt($("#" + target_div).css("grid-column")) - 1;
        ligne_vide = parseInt($("#" + target_div).css("grid-row")) - 1;
        
        //give right
        give_movement_right(col_vide, ligne_vide);

        dest_div.appendChild(document.getElementById(target_div).firstChild);
        let deplacements=$("#deplacements");
        deplacements.text(parseInt(deplacements.text())+1);
        if(verifierVictoire()){
            alert("VICTOIRE AVEC "+deplacements.text()+" DEPLACEMENTS!");
        }
    } else {
        alert("MOVEMENT ILLEGAL!!!!")
    }

}

function give_movement_right(col, ligne) {

    //alert("pos_"+add(ligne,-1)+"_"+col);
    let upBlock;
    let downBlock;
    let rightBlock;
    let leftBlock;

    try {


        upBlock = document.getElementById("pos_" + add(ligne, 1) + "_" + col);
        upBlock.onclick = function () {
            move(this.id);
        };

    } catch (e) {

    }
    try {


        downBlock = document.getElementById("pos_" + add(ligne, -1) + "_" + col);
        downBlock.onclick = function () {
            move(this.id);
        };

    } catch (e) {

    }
    try {


        leftBlock = document.getElementById("pos_" + ligne + "_" + add(col, 1));
        leftBlock.onclick = function () {
            move(this.id);
        };

    } catch (e) {

    }
    try {


        rightBlock = document.getElementById("pos_" + ligne + "_" + add(col, -1));
        rightBlock.onclick = function () {
            move(this.id);
        };

    } catch (e) {

    }
    document.onkeydown = function (event) {
        try {


            if ("ArrowDown" === event.key) {
                move(downBlock.id);
            } else if ("ArrowUp" === event.key) {
                move(upBlock.id);
            } else if ("ArrowLeft" === event.key) {
                move(leftBlock.id);
            } else if ("ArrowRight" === event.key) {
                move(rightBlock.id);
            }
        } catch (e) {
            console.log("attempt to move off limit \n");
        }
    };
    //prend ligne et col vide et met en place les listner pour activer le deplacement au case qui ont le droit
}

function remove_movement_right(col, ligne) {
    //prend ligne et col vide et retire les listner pour activer le deplacement au case qui avaient le droit
    try {
        document.getElementById("pos_" + add(ligne, 1) + "_" + col).onclick = null;
    } catch (e) {

    }
    try {
        document.getElementById("pos_" + add(ligne, -1) + "_" + col).onclick = null;
    } catch (e) {

    }
    try {
        document.getElementById("pos_" + ligne + "_" + add(col, 1)).onclick = null;
    } catch (e) {

    }
    try {
        document.getElementById("pos_" + ligne + "_" + add(col, -1)).onclick = null;
    } catch (e) {

    }
}

function add(a, b) {
    return parseInt(a) + parseInt(b);
}

function brasser() {
    //melanger puzzle puis verifier resolvabilite recommencer si resolvable retourne false
    let game = document.getElementById("main_game");
    let children = game.childNodes;
    for (i = 0; i < game.childElementCount; i++) {
        let rand = Math.floor(Math.random() * (game.childElementCount));
        if(children[rand].hasChildNodes() && children[i].hasChildNodes()){
            children[rand].appendChild(children[i].firstChild);
            children[i].appendChild(children[rand].firstChild);}

    }

    if (!resolvable()) {
        brasser()
    }

}

function resolvable() {
    // "theroreme" et preuve de resolvabilite par Mark Ryan
    // http://www.cs.bham.ac.uk/~mdr/teaching/modules04/java2/TilesSolvability.html
    //je ne sais pas si ca marche pour les cas ou nbcol != nblignes

    let inversions = 0;
    let game = document.getElementById("main_game");
    let children = game.childNodes;

    for (i = 0; i < game.childElementCount; i++) {
        for (j = i + 1; j < game.childElementCount; j++) {
            try {
                if (parseInt(children[j].firstElementChild.title) > parseInt(children[i].firstElementChild.title)
                    && parseInt(children[j].firstElementChild.title) !== null && parseInt(children[i].firstElementChild.title) !== null) {
                    inversions++;
                }
            } catch (e) {

            }
        }
    }

    if(parseInt(document.getElementById("nbcol").value)%2===1)
    {
        return !(inversions % 2 === 1);
    }else{
        if((parseInt(document.getElementById("nbligne")).value-ligne_vide)%2===0){
            return !(inversions % 2 === 1);
        }else {
            return (inversions % 2 === 1);
        }
    }

}


function verifierVictoire() {
    let game = document.getElementById("main_game");
    let children = game.childNodes;
    for (i = 0; i < game.childElementCount; i++) {

        if(children[i].firstChild !== null && parseInt(children[i].title) !== parseInt(children[i].firstElementChild.title )  ){
            return false;
        }
    }
    return true;
}
